﻿using Lecture1_LINQ.Entities;
using Lecture1_LINQ.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture1_LINQ
{
    public static class ConsoleMenu
    {
        private static List<Option> options = new List<Option>
            {
                new Option("1. Count tasks of current user", () => CountTasksOfCurrentUser()),
                new Option("2. Get all tasks of user with name length < 45", () =>  GetTasksOfCurrentUser()),
                new Option("3. Get finished this year tasks of user", () =>  GetFinishedThisYearTasksOfCurrentUser()),
                new Option("5. Sort by user first name and task name", () =>  GetSortedUserByAscendingAndTasksByDescending()),
                new Option("6. Analyze user's last project and tasks", () =>  AnalyzeUserProjectsAndTasks()),
                new Option("7. Analyze project's tasks and team", () =>  AnalyzeProjectTasksAndTeam()),
                new Option("Exit", () => Environment.Exit(0)),
            };

        public static void CountTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var query = DataStorage.Projects
                            .Where(p => p.AuthorId == userID)
                            .ToDictionary(k => k.Id, v => v.Tasks.Count);
            foreach(var item in query)
            {
                Console.WriteLine($"Projectid: {item.Key}, Tasks: {item.Value}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var query = DataStorage.Tasks
                .Where(t => t.PerformerId == userID)
                .Where(t => t.Name.Length < 45)
                .ToList();
            Console.WriteLine();
            foreach(var item in query)
            {
                Console.WriteLine($"\n\nName: {item.Name}\nDescription: {item.Description}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetFinishedThisYearTasksOfCurrentUser()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var query = DataStorage.Tasks
                .Where(t => t.PerformerId == userID)
                .Where(t => t.FinishedAt.HasValue)
                .Where(t => t.FinishedAt.Value.Year == DateTime.Now.Year)
                .Select((task) => new { task.Id, task.Name })
                .ToList();
            Console.WriteLine();
            foreach (var item in query)
            {
                Console.WriteLine($"\n\nTask ID: {item.Id}\nTask Name: {item.Name}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void GetSortedUserByAscendingAndTasksByDescending()
        {
            Console.Clear();
            var query = DataStorage.Users
                .Select(u => new User
                {
                    BirthDay = u.BirthDay,
                    Email = u.Email,
                    FirstName = u.FirstName,
                    Id = u.Id,
                    LastName = u.LastName,
                    RegisteredAt = u.RegisteredAt,
                    TeamId = u.TeamId,
                    Team = u.Team,
                    Tasks = u.Tasks.OrderByDescending(t => t.Name.Length).ToList()
                })
                .OrderBy(x => x.FirstName);
            Console.WriteLine();
            foreach (var item in query)
            {
                Console.WriteLine($"\n\nUser First Name: {item.FirstName}. Tasks: \n");
                foreach(var task in item.Tasks)
                {
                    Console.WriteLine(task.Name);
                }
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void AnalyzeUserProjectsAndTasks()
        {
            Console.Clear();
            Console.WriteLine("Write user ID: ");
            Int32.TryParse(Console.ReadLine(), out int userID);
            var lastProject = DataStorage.Projects.Where(p => p.AuthorId == userID).OrderByDescending(p => p.CreatedAt).FirstOrDefault();
            var query = DataStorage.Users
                .Where(u => u.Id == userID)
                .Select(u => new UserWithAdditionalInfo
                {
                    User = u,
                    LastProject = lastProject,
                    TotalTasksCount = lastProject.Tasks.Count,
                    TotalUncompletedAndCanceledTasks = lastProject.Tasks.Where(t => !t.FinishedAt.HasValue).Count(),
                    LongestTask = lastProject.Tasks
                                    .Where(t => t.FinishedAt.HasValue)
                                    .OrderByDescending(t => t.FinishedAt.Value.Ticks - t.CreatedAt.Ticks)
                                    .FirstOrDefault()
                });
            Console.WriteLine(); 
            foreach (var item in query)
            {
                Console.WriteLine($"\n\nUser: {item.User.FirstName} {item.User.LastName}");
                Console.WriteLine($"Last Project: {item.LastProject?.Name}");
                Console.WriteLine($"Total tasks count: {item.TotalTasksCount}");
                Console.WriteLine($"Uncompleted and canceled tasks count: {item.TotalUncompletedAndCanceledTasks}");
                Console.WriteLine($"Longest task: {item.LongestTask?.Name}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static void AnalyzeProjectTasksAndTeam()
        {
            Console.Clear();
            Console.WriteLine("Write project ID: ");
            Int32.TryParse(Console.ReadLine(), out int projectID);
            var query = DataStorage.Projects
                .Where(p => p.Id == projectID)
                .Select(p => new ProjectWithAdditionalInformation
                {
                    Project = p,
                    LongestTaskByDescription = p.Tasks
                                                .OrderByDescending(t => t.Description.Length)
                                                .FirstOrDefault(),
                    ShortestTaskByName = p.Tasks
                                            .OrderBy(t => t.Name.Length)
                                            .FirstOrDefault(),
                    TotalTeamCount = p.Description.Length > 20 ? p.Team.Users.Count : p.Tasks.Count < 3 ? p.Team.Users.Count : 0
                });
            Console.WriteLine();
            foreach (var item in query)
            {
                Console.WriteLine($"\n\nProject: {item.Project?.Name}");
                Console.WriteLine($"Longest task by description: {item.LongestTaskByDescription?.Description}");
                Console.WriteLine($"Shortest task by name: {item.ShortestTaskByName?.Name}");
                Console.WriteLine($"Total team count where project description length > 20 or tasks count < 3: {item.TotalTeamCount}");
            }
            Console.WriteLine("\n\nPress any key to go back...");
            Console.ReadKey();
        }

        public static async void InitializeData()
        {
            await DataStorage.Initialize();
            var users = DataStorage.Users;
            var projects = DataStorage.Projects;
            var tasks = DataStorage.Tasks;
            var teams = DataStorage.Teams;
        }

        public static void Start()
        {

            /*try
            {*/
            int index = 0;
                ConsoleKeyInfo keyinfo;
                do
                {
                    WriteMenu(options, options[index]);
                    keyinfo = Console.ReadKey();

                    // Handle each key input (down arrow will write the menu again with a different selected item)
                    if (keyinfo.Key == ConsoleKey.DownArrow)
                    {
                        if (index + 1 < options.Count)
                        {
                            index++;
                            WriteMenu(options, options[index]);
                        }
                    }
                    if (keyinfo.Key == ConsoleKey.UpArrow)
                    {
                        if (index - 1 >= 0)
                        {
                            index--;
                            WriteMenu(options, options[index]);
                        }
                    }
                    // Handle different action for the option
                    if (keyinfo.Key == ConsoleKey.Enter)
                    {
                        options[index].Selected.Invoke();
                        index = 0;
                    }
                }
                while (keyinfo.Key != ConsoleKey.X);
            //}
            /*catch (Exception e)
            {
                Console.WriteLine($"You can't perfom this operation: {e.Message}");
                Console.ReadKey();
                return;
            }*/

            Console.ReadKey();
        }

        static void WriteMenu(List<Option> options, Option selectedOption)
        {
            Console.Clear();

            foreach (Option option in options)
            {
                if (option == selectedOption)
                {
                    Console.Write("> ");
                }
                else
                {
                    Console.Write(" ");
                }

                Console.WriteLine(option.Name);
            }
        }
        protected class Option
        {
            public string Name { get; }
            public Action Selected { get; }

            public Option(string name, Action selected)
            {
                Name = name;
                Selected = selected;
            }
        }
    }

}
