﻿using Lecture1_LINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_LINQ.Services
{
    class TeamsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TeamsHttpService()
        {
            _httpClient.BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/Teams/");
        }
        public async Task<List<Team>> GetAllTeams()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Team>>(content);
        }

        public async Task<Team> GetTeamById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Team>(content);
        }
    }
}
