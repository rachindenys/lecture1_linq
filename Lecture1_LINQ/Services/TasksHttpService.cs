﻿using Lecture1_LINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace Lecture1_LINQ.Services
{
    class TasksHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public TasksHttpService()
        {
            _httpClient.BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/Tasks/");
        }
        public async Task<List<Entities.Task>> GetAllTasks()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Entities.Task>>(content);
        }

        public async Task<Entities.Task> GetTaskById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Entities.Task>(content);
        }
    }
}
