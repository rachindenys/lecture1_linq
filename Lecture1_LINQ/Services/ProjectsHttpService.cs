﻿using Lecture1_LINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_LINQ.Services
{
    class ProjectsHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public ProjectsHttpService()
        {
            _httpClient.BaseAddress = new Uri($"https://bsa21.azurewebsites.net/api/Projects/");
        }
        public async Task<List<Project>> GetAllProjects()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<Project>>(content);
        }

        public async Task<Project> GetProjectById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<Project>(content);
        }
    }
}
