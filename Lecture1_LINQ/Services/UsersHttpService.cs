﻿using Lecture1_LINQ.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Lecture1_LINQ.Services
{
    class UsersHttpService
    {
        private readonly HttpClient _httpClient = new HttpClient();


        public UsersHttpService()
        {
            _httpClient.BaseAddress = new Uri("https://bsa21.azurewebsites.net/api/Users/");
        }
        public async Task<List<User>> GetAllUsers()
        {
            var content = await _httpClient.GetStringAsync("");
            return JsonConvert.DeserializeObject<List<User>>(content);
        }

        public async Task<User> GetUserById(int id)
        {
            var content = await _httpClient.GetStringAsync($"{id}");
            return JsonConvert.DeserializeObject<User>(content);
        }
    }
}
