﻿using Lecture1_LINQ.Entities;
using Lecture1_LINQ.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lecture1_LINQ
{
    public static class DataStorage
    {
        private static ProjectsHttpService _projectsService = new ProjectsHttpService();
        private static TasksHttpService _tasksService = new TasksHttpService();
        private static TeamsHttpService _teamsService = new TeamsHttpService();
        private static UsersHttpService _usersService = new UsersHttpService();
        public static List<Project> Projects { get; set; }
        public static List<Task> Tasks { get; set; }
        public static List<Team> Teams { get; set; }
        public static List<User> Users { get; set; }


        public static async System.Threading.Tasks.Task Initialize()
        {
            var projects = await _projectsService.GetAllProjects();
            var tasks = await _tasksService.GetAllTasks();
            var teams = await _teamsService.GetAllTeams();
            var users = await _usersService.GetAllUsers();
            var connectedTeams = teams
                    .GroupJoin(users,
                    t => t.Id,
                    u => u.TeamId,
                    (t, u) =>
                    {
                        return new Team
                        {
                            Id = t.Id,
                            Name = t.Name,
                            CreatedAt = t.CreatedAt,
                            Users = u.ToList()
                        };
                    });
            var connectedTasks = tasks
                .Join(users,
                t => t.PerformerId,
                u => u.Id,
                (t, u) =>
                {
                    return new Task
                    {
                        Id = t.Id,
                        Name = t.Name,
                        PerformerId = t.PerformerId,
                        ProjectId = t.ProjectId,
                        State = t.State,
                        CreatedAt = t.CreatedAt,
                        FinishedAt = t.FinishedAt,
                        Description = t.Description,
                        Performer = u
                    };
                });
            var connectedProjects = projects
                .Join(users,
                p => p.AuthorId,
                u => u.Id,
                (p, u) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        CreatedAt = p.CreatedAt,
                        Author = u
                    };
                })
                .Join(connectedTeams,
                p => p.TeamId,
                t => t.Id,
                (p, t) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        CreatedAt = p.CreatedAt,
                        Team = t
                    };
                })
                .GroupJoin(connectedTasks,
                p => p.Id,
                t => t.ProjectId,
                (p, t) =>
                {
                    return new Project
                    {
                        AuthorId = p.AuthorId,
                        Deadline = p.Deadline,
                        Description = p.Description,
                        Id = p.Id,
                        Name = p.Name,
                        TeamId = p.TeamId,
                        Author = p.Author,
                        Team = p.Team,
                        CreatedAt = p.CreatedAt,
                        Tasks = t.ToList()
                    };
                });
            var connectedUsers = users
                .Join(teams,
                u => u.TeamId,
                t => t.Id,
                (u, t) =>
                {
                    return new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        Id = u.Id,
                        LastName = u.LastName,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = t
                    };
                })
                .Union(users.Where(u => u.TeamId == null))
                .GroupJoin(tasks,
                u => u.Id,
                t => t.PerformerId,
                (u, t) => 
                {
                    return new User
                    {
                        BirthDay = u.BirthDay,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        Id = u.Id,
                        LastName = u.LastName,
                        RegisteredAt = u.RegisteredAt,
                        TeamId = u.TeamId,
                        Team = u.Team,
                        Tasks = t.ToList()
                    };
                });
            Tasks = connectedTasks.ToList();
            Projects = connectedProjects.ToList();
            Teams = connectedTeams.ToList();
            Users = connectedUsers.ToList();
        }
    }
}
