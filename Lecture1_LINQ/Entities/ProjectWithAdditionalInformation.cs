﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture1_LINQ.Entities
{
    public class ProjectWithAdditionalInformation
    {
        public Project Project { get; set; }
        public Task? LongestTaskByDescription { get; set; }
        public Task? ShortestTaskByName { get; set; }
        public int TotalTeamCount { get; set; }
    }
}
