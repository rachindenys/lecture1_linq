﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture1_LINQ.Entities
{
    public enum TaskState
    {
        First = 0,
        Second = 1,
        Third = 2,
        Fourth = 3
    }
}
