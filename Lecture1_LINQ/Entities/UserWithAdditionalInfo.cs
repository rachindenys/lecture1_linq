﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lecture1_LINQ.Entities
{
    class UserWithAdditionalInfo
    {
        public User User { get; set; }
        public Project LastProject { get; set; }
        public int TotalTasksCount { get; set; }
        public int TotalUncompletedAndCanceledTasks { get; set; }
        public Task LongestTask { get; set; }
    }
}
